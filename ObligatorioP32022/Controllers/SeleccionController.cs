﻿using LogicaDatos.Datos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ObligatorioP32022.Models.SeleccionModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using ObligatorioP32022.Models.MapeoSeleccion;
using LogicaAplicacion.InterfacesCasosUso;

namespace ObligatorioP32022.Controllers
{
    public class SeleccionController : Controller
    {
        private HttpClient _clienteApi = new HttpClient();
        private JsonSerializerOptions _options = new JsonSerializerOptions 
        {
            PropertyNameCaseInsensitive = true,
        };

        public SeleccionController()
        {
            _clienteApi.BaseAddress = new Uri(@"http://localhost:61617/api/seleccion");
            _clienteApi.DefaultRequestHeaders.Accept.Clear();
            _clienteApi.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(@"application/json"));
        }

        // GET: SeleccionController
        public ActionResult Index()
        {
            return View();
        }

        // GET: SeleccionController
        public ActionResult DisplayAllSeleccion()
        {
            //pegarle a una api para obtener todos las selecciones(usando SeleccionMuestraModel)
            //y mostrarlas en una vista
            var response = _clienteApi.GetAsync(_clienteApi.BaseAddress);
            if (response.Result.IsSuccessStatusCode)
            {
                var stream = response.Result.Content.ReadAsStringAsync().Result;
                var selecciones = JsonSerializer.Deserialize<IEnumerable<Seleccion>>(stream, _options);
                //turn selecciones into SeleccionMuestraModel
                IEnumerable<SeleccionMuestraModel> seleccionesVista = selecciones.Select
                    (
                        //use MapeoSeleccionModelo
                        s => MapeoSeleccionModelo.FromSeleccionToSeleccionMuestra(s)
                    );


                return View(seleccionesVista);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, (int)response.Result.StatusCode + "Error al obtener las selecciones");
            }
            
        }
        

        // GET: SeleccionController/Details/5 
        public ActionResult Details(int id)
        {
            var response = _clienteApi.GetAsync(_clienteApi.BaseAddress + "/" + id);
            if (response.Result.IsSuccessStatusCode)
            {
                var stream = response.Result.Content.ReadAsStringAsync().Result;
                var seleccion = JsonSerializer.Deserialize<Seleccion>(stream, _options);
                //turn seleccion into SeleccionMuestraModel
                var seleccionMuestra = MapeoSeleccionModelo.FromSeleccionToSeleccionMuestra(seleccion);
                return View(seleccionMuestra);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error al obtener la seleccion");
            }
        }

        // GET: SeleccionController/Create // consume api to get Paises and return in ViewBag Paises
        public ActionResult Create()
        {
            IEnumerable<Pais> paises = null;
            //consume api with this uri http://localhost:61617/api/pais to get all paises in a variable paises
            var responsePaises = _clienteApi.GetAsync(@"http://localhost:61617/api/pais");
            if (responsePaises.Result.IsSuccessStatusCode)
            {
                var stream = responsePaises.Result.Content.ReadAsStringAsync().Result;
                paises = JsonSerializer.Deserialize<IEnumerable<Pais>>(stream, _options);
            }
            ViewBag.paises = paises.ToList();
            ViewBag.grupos = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H" };
            return View();
        }

        // POST: SeleccionController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SeleccionAltaModel seleccionAltaModel)
        {
            try
            {
                //turn SeleccionAltaModel into Seleccion
                var seleccion = MapeoSeleccionModelo.FromSeleccionAltaToSeleccion(seleccionAltaModel);
                //post to api
                var response = _clienteApi.PostAsync(_clienteApi.BaseAddress, new StringContent(JsonSerializer.Serialize(seleccion), System.Text.Encoding.UTF8, "application/json"));
                if (response.Result.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(DisplayAllSeleccion));
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, (int)response.Result.StatusCode + " Error al crear la seleccion");
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: SeleccionController/Edit/5
        public ActionResult Edit(int id)
        {
            IEnumerable<Pais> paises = null;
            //consume api with this uri http://localhost:61617/api/pais to get all paises in a variable paises
            var responsePaises = _clienteApi.GetAsync(@"http://localhost:61617/api/pais");
            if (responsePaises.Result.IsSuccessStatusCode)
            {
                var stream = responsePaises.Result.Content.ReadAsStringAsync().Result;
                paises = JsonSerializer.Deserialize<IEnumerable<Pais>>(stream, _options);
            }
            ViewBag.paises = paises.ToList();

            ViewBag.grupos = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H" };
            var response = _clienteApi.GetAsync(_clienteApi.BaseAddress + "/" + id);
            if (response.Result.IsSuccessStatusCode)
            {
                var stream = response.Result.Content.ReadAsStringAsync().Result;
                var seleccion = JsonSerializer.Deserialize<Seleccion>(stream, _options);
                //turn seleccion into SeleccionEdit
                var seleccionEdit = MapeoSeleccionModelo.FromSeleccionToSeleccionEdit(seleccion);
                return View(seleccionEdit);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error al obtener la seleccion");
            }
        }

        // POST: SeleccionController/Edit/5
        [HttpPost]
        public ActionResult Edit(SeleccionEditModel seleccionEdit)
        {
            try
            {
                //turn SeleccionEditModel into Seleccion
                var seleccion = MapeoSeleccionModelo.FromSeleccionEditToSeleccion(seleccionEdit);
                //post to api
                var response = _clienteApi.PutAsync(_clienteApi.BaseAddress + "/" + seleccion.ID, new StringContent(JsonSerializer.Serialize(seleccion), System.Text.Encoding.UTF8, "application/json"));
                if (response.Result.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(DisplayAllSeleccion));
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Error al editar la seleccion");
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: SeleccionController/Delete/5 
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, "Error al obtener la seleccion");
            }
            
            var response = _clienteApi.GetAsync(_clienteApi.BaseAddress + "/" + id);
            if (response.Result.IsSuccessStatusCode)
            {
                var stream = response.Result.Content.ReadAsStringAsync().Result;
                var seleccion = JsonSerializer.Deserialize<Seleccion>(stream, _options);
                //turn seleccion into SeleccionMuestraModel
                var seleccionMuestra = MapeoSeleccionModelo.FromSeleccionToSeleccionMuestra(seleccion);
                return View(seleccionMuestra);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error al obtener la seleccion");
            }
        }

        // POST: SeleccionController/Delete/5 
        [HttpPost, ActionName("Delete")]
        public ActionResult DeletePost(int id)
        {
            try
            {
                var response = _clienteApi.DeleteAsync(_clienteApi.BaseAddress + "/" + id);
                if (response.Result.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(DisplayAllSeleccion));
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, "Error al eliminar la seleccion");
                }
            }
            catch
            {
                return View();
            }
        }
        //finally order viewmodel list
        //GET: SeleccionController/ClasificacionGrupo
        [HttpGet]
        public ActionResult CalificacionGrupo(string grupo)
        {
            ViewBag.grupos = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H" };
            if (grupo == null)
            {
                grupo = "B";
            }
            var response = _clienteApi.GetAsync(_clienteApi.BaseAddress + "/selecciondegrupo/" + grupo);
            if (response.Result.IsSuccessStatusCode)
            {
                var stream = response.Result.Content.ReadAsStringAsync().Result;
                var selecciones = JsonSerializer.Deserialize<IEnumerable<Seleccion>>(stream, _options);
                List<SeleccionResultadosModel> viewModel = new List<SeleccionResultadosModel>();

                foreach(var seleccion in selecciones)
                {
                    var puntaje = 0;
                    response = _clienteApi.GetAsync(_clienteApi.BaseAddress + "/puntaje/" + seleccion.ID);
                    if (response.Result.IsSuccessStatusCode)
                    {
                        stream = response.Result.Content.ReadAsStringAsync().Result;
                        puntaje = JsonSerializer.Deserialize<int>(stream, _options);
                    }

                    var golesAFavor = 0;
                    response = _clienteApi.GetAsync(_clienteApi.BaseAddress + "/golesfavor/" + seleccion.ID);
                    if (response.Result.IsSuccessStatusCode)
                    {
                        stream = response.Result.Content.ReadAsStringAsync().Result;
                        golesAFavor = JsonSerializer.Deserialize<int>(stream, _options);
                    }

                    var golesEnContra = 0;
                    response = _clienteApi.GetAsync(_clienteApi.BaseAddress + "/golescontra/" + seleccion.ID);
                    if (response.Result.IsSuccessStatusCode)
                    {
                        stream = response.Result.Content.ReadAsStringAsync().Result;
                        golesEnContra = JsonSerializer.Deserialize<int>(stream, _options);
                    }

                    viewModel.Add(new SeleccionResultadosModel(seleccion.Nombre, puntaje, golesAFavor, golesEnContra));
                }
                 viewModel.Sort((vmo1, vmo2) => vmo2.Puntaje.CompareTo(vmo1.Puntaje));




                return View(viewModel);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Error al obtener la seleccion");
            }

        }
    }
}
