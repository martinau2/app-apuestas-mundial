﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http.Headers;
using System.Text.Json;
using System;
using System.Net.Http;
using LogicaDatos.Datos;
using ObligatorioP32022.Models.SeleccionModel;
using System.Collections.Generic;
using ObligatorioP32022.Models.PartidoModel;
using System.Linq;
using ObligatorioP32022.Models.MapeoSeleccion;

namespace ObligatorioP32022.Controllers
{
    public class PartidoController : Controller
    {
        private HttpClient _clienteApi = new HttpClient();
        private JsonSerializerOptions _options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
        };

        public PartidoController()
        {
            _clienteApi.BaseAddress = new Uri(@"http://localhost:61617/api/partido");
            _clienteApi.DefaultRequestHeaders.Accept.Clear();
            _clienteApi.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(@"application/json"));
        }

        // GET: PartidoController
        [HttpGet]
        public ActionResult GetPartidoBy(PartidoGetByModel partidoGetByModel)
        {
            ViewBag.grupos = new List<string> { "A", "B", "C", "D", "E", "F", "G", "H" };
            //Pegarle a la api para recibir HttpGet los partidos usando partidoGetByModel como body 
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri(_clienteApi.BaseAddress + "/GetPartidos"),
                Content = new StringContent(JsonSerializer.Serialize(partidoGetByModel), System.Text.Encoding.UTF8, "application/json")
            };
            var response = _clienteApi.SendAsync(request);
            if (response.Result.IsSuccessStatusCode)
            {
                var stream = response.Result.Content.ReadAsStringAsync().Result;
                var partidos = JsonSerializer.Deserialize<IEnumerable<Partido>>(stream, _options);

                IEnumerable<PartidoShowModel> partidosVista = partidos.Select
                    (
                        //use MapeoSeleccionModelo
                        p => MapeoPartidoModel.GetPartidoShowModelFromPartido(p)
                    );


                return View(partidosVista);
            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
                
            }





        }

    }
}
