using LogicaAplicacion.InterfacesCasosUso;
using LogicaDatos.Datos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ObligatorioP32022.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using LogicaAplicacion.InterfacesCasosUso.Regiones;
using LogicaAplicacion.CasosUso.Regiones;

namespace ObligatorioP32022.Controllers
{
    public class PaisController : Controller
    {
        private IPaisCasoUso _paisCasoUso;

        private IRegionCasoUso _regionCasoUso;
        private IWebHostEnvironment _env;

        public PaisController(IPaisCasoUso paisCasoUso, IRegionCasoUso findAllRegion, IWebHostEnvironment env)
        {
            _regionCasoUso = findAllRegion;
            _paisCasoUso = paisCasoUso;
            _env = env;
        }
        

        public IActionResult DisplayAllPaises()
        {
            {
                var paises = _paisCasoUso.FindAll().ToArray();
                var regiones = _regionCasoUso.FindAll();

                var nombreRegiones = new List<string>();
                foreach (var reg in regiones)
                {
                    nombreRegiones.Add(reg.Nombre);
                }
                ViewBag.Regiones = nombreRegiones;

                var idRegiones = new List<int>();
                foreach (var reg in regiones)
                {
                    idRegiones.Add(reg.ID);
                }
                ViewBag.IdRegiones = idRegiones;


                return View(paises);
            }
        }
        [HttpPost]
        public IActionResult DisplayAllPaises(int? id,string codigo,int? region)
        {
            var paises = _paisCasoUso.FindForSearch(id, codigo, region).ToArray();
            var regiones = _regionCasoUso.FindAll();
            
            var nombreRegiones = new List<string>();
            foreach (var reg in regiones)
            {
                nombreRegiones.Add(reg.Nombre);
            }
            ViewBag.Regiones = nombreRegiones;

            var idRegiones = new List<int>();
            foreach (var reg in regiones)
            {
                idRegiones.Add(reg.ID);
            }
            ViewBag.IdRegiones = idRegiones;
            return View(paises);

        }

        public ActionResult Create()
        {
            var regiones = _regionCasoUso.FindAll();

            var nombreRegiones = new List<string>();
            foreach (var region in regiones)
            {
                nombreRegiones.Add(region.Nombre);
            }
            ViewBag.Regiones = nombreRegiones;

            var idRegiones = new List<int>();
            foreach (var region in regiones)
            {
                idRegiones.Add(region.ID);
            }
            ViewBag.IdRegiones = idRegiones;

            return View();
        }

        public ActionResult Details(int id)
        {
            var pais = _paisCasoUso.FindById(id);
            return View(pais);
        }


        [HttpPost]
        public ActionResult Create(Pais pais,IFormFile foto, int regionID)
        {
            try
            {
                
                if (GuardarImagen(foto, pais))
                {
                    pais.RegionID = regionID;
                    _paisCasoUso.InsertarPais(pais);
                    return RedirectToAction(nameof(DisplayAllPaises));
                }
                else
                {
                    return Create();
                }
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = ex.Message;
                return Create();
            }
        }

        private bool GuardarImagen(IFormFile imagen, Pais p)
        {
            if (imagen == null || p == null)
                return false;
            // SUBIR LA IMAGEN
            string rutaFisicaWwwRoot = _env.WebRootPath;
            //ruta donde se guardan las fotos de las personas
            //ruta donde se guardan las fotos de las personas
            string fileType = imagen.FileName.Split(".")[1];

            if (fileType != "png")
            {
                return false;
            }

            string codigo = p.Codigo;

            string nombreImagen = codigo + ".png";

            string rutaFisicaFoto = Path.Combine(rutaFisicaWwwRoot, "imagenes", "fotos", nombreImagen);
            try
            {
                //el m�todo using libera los recursos del objeto FileStream al finalizar
                using (FileStream f = new FileStream(rutaFisicaFoto, FileMode.Create))
                {
                    //si fueran archivos grandes o si fueran varios, deber�amos usar la versi�n
                    //asincr�nica de CopyTo, aqu� no es necesario.
                    //ser�a: await imagen.CopyToAsync (f);
                    imagen.CopyTo(f);
                }
                //GUARDAR EL NOMBRE DE LA IMAGEN SUBIDA EN EL OBJETO
                p.Imagen = nombreImagen;
                return true;
            }
            catch 
            {
                return false;
            }
        }


        public ActionResult Edit(int id)
        {
            var pais = _paisCasoUso.FindById(id);
            var regiones = _regionCasoUso.FindAll();

            var nombreRegiones = new List<string>();
            foreach (var region in regiones)
            {
                nombreRegiones.Add(region.Nombre);
            }
            ViewBag.Regiones = nombreRegiones;

            var idRegiones = new List<int>();
            foreach (var region in regiones)
            {
                idRegiones.Add(region.ID);
            }
            ViewBag.IdRegiones = idRegiones;

            return View(pais);

        }

        [HttpPost]
        public ActionResult Edit(Pais pais, IFormFile foto, int regionID)
        {
            try
            {
                if (foto != null)
                {
                    if (GuardarImagen(foto, pais))
                    {
                        pais.RegionID = regionID;
                        _paisCasoUso.ModificarPais(pais);
                        return RedirectToAction(nameof(DisplayAllPaises));
                    }
                    else
                    {
                        return Edit(pais.ID);
                    }
                }
                else
                {
                    pais.RegionID = regionID;
                    _paisCasoUso.ModificarPais(pais);
                    return RedirectToAction(nameof(DisplayAllPaises));
                }
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = ex.Message;
                return Edit(pais.ID);
            }
        }

        public ActionResult Delete(int id)
        {
            var pais = _paisCasoUso.FindById(id);
            ViewBag.Region = _regionCasoUso.FindById(pais.RegionID).Nombre;
            return View(pais);
        }

        [HttpPost]
        public ActionResult Delete(Pais pais)
        {
            try
            {
                _paisCasoUso.EliminarPais(pais);
                return RedirectToAction(nameof(DisplayAllPaises));
            }
            catch (Exception ex)
            {
                ViewBag.Mensaje = ex.Message;
                return Delete(pais.ID);
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
