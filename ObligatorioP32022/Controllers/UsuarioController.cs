﻿using LogicaAplicacion.InterfacesCasosUso.Regiones;
using LogicaAplicacion.InterfacesCasosUso;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LogicaAplicacion.InterfacesCasosUso.UsuarioI;
using ManejoUsuario;
using System;

namespace ObligatorioP32022.Controllers
{
    public class UsuarioController : Controller
    {
        private IUsuarioCasoUso _usuarioCasoUso;

        public UsuarioController(IUsuarioCasoUso usuarioCasoUso)
        {
            _usuarioCasoUso = usuarioCasoUso;
        }
        // GET: UsuarioController
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Email, string Password)
        {
            var usuario = _usuarioCasoUso.Login(Email,Password);
            if (usuario == null)
            {
                ViewBag.error = "Usuario o Password incorrectos";
                return View();
            }
            else
            {
                HttpContext.Session.SetString("usuario", usuario.Email);
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Usuario usuario)
        {
            try
            {
                var usuarioCreado = _usuarioCasoUso.Register(usuario);
                if(usuarioCreado != null)
                {
                    return RedirectToAction("Login");
                }
                else
                {
                    ViewBag.error = "Error en la password o en el usuario";
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View();
            }
        }

    }
}
