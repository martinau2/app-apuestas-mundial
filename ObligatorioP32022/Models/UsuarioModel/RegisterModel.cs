﻿namespace ObligatorioP32022.Models.UsuarioModel
{
    public class RegisterModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
