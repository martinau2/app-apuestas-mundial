﻿using LogicaDatos.Datos;
using ObligatorioP32022.Models.PartidoModel;

namespace ObligatorioP32022.Models.MapeoSeleccion
{
    public static class MapeoPartidoModel
    {
        public static PartidoShowModel GetPartidoShowModelFromPartido(Partido p)
        {
            {
                PartidoShowModel partidoShowModel = new PartidoShowModel();
                partidoShowModel.NombreSeleccion1 = p.Seleccion1.Nombre;
                partidoShowModel.NombreSeleccion2 = p.Seleccion2.Nombre;
                if (p.Resultado1 != null && p.Resultado2 != null)
                {
                    partidoShowModel.GolesSeleccion1 = p.Resultado1.GolesSeleccion;
                    partidoShowModel.AmarillasSeleccion1 = p.Resultado1.CantAmarillas;
                    partidoShowModel.RojasSeleccion1 = p.Resultado1.CantRojas;
                    partidoShowModel.GolesSeleccion2 = p.Resultado2.GolesSeleccion;
                    partidoShowModel.PuntajeSeleccion1 = p.PuntajeSeleccion1;
                    partidoShowModel.AmarillasSeleccion2 = p.Resultado2.CantAmarillas;
                    partidoShowModel.RojasSeleccion2 = p.Resultado2.CantRojas;
                    partidoShowModel.PuntajeSeleccion2 = p.PuntajeSeleccion2;
                }
                partidoShowModel.FechaYHora = p.FechaYHora;
                return partidoShowModel;
            }
        }
    }
}
