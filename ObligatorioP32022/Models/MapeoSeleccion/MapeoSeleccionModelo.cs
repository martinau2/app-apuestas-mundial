﻿using LogicaDatos.Datos;
using ObligatorioP32022.Models.SeleccionModel;
using System;

namespace ObligatorioP32022.Models.MapeoSeleccion
{
    public static class MapeoSeleccionModelo
    {
        public static Seleccion ModeloASeleccion(SeleccionAltaModel modelo)
        {
            Seleccion seleccion = new Seleccion();
            seleccion.PaisId = modelo.PaisId;
            seleccion.Nombre = modelo.Nombre;
            seleccion.Email = modelo.Email;
            seleccion.Telefono = modelo.Telefono;
            seleccion.CantApostadores = modelo.CantApostadores;
            seleccion.Grupo = new GrupoSeleccion() {Nombre = modelo.Grupo };
            return seleccion;
        }

        public static SeleccionMuestraModel FromSeleccionToSeleccionMuestra(Seleccion seleccion)
        {
            SeleccionMuestraModel muestra = new SeleccionMuestraModel();
            muestra.id = seleccion.ID;
            muestra.NombrePais = seleccion._Pais.Nombre;
            muestra.nombreUrlBandera = seleccion._Pais.Imagen;
            muestra.CodigoPais = seleccion._Pais.Codigo;
            muestra.Nombre = seleccion.Nombre;
            muestra.Email = seleccion.Email;
            muestra.Telefono = seleccion.Telefono;
            muestra.CantApostadores = seleccion.CantApostadores;
            muestra.Grupo = seleccion.Grupo.Nombre;
            return muestra;
        }
        
        public static Seleccion FromSeleccionAltaToSeleccion(SeleccionAltaModel seleccionAltaModel)
        {
            Seleccion seleccion = new Seleccion();
            seleccion.PaisId = seleccionAltaModel.PaisId;
            seleccion.Nombre = seleccionAltaModel.Nombre;
            seleccion.Email = seleccionAltaModel.Email;
            seleccion.Telefono = seleccionAltaModel.Telefono;
            seleccion.CantApostadores = seleccionAltaModel.CantApostadores;
            seleccion.Grupo = new GrupoSeleccion() { Nombre = seleccionAltaModel.Grupo };
            return seleccion;
        }

        public static Seleccion FromSeleccionEditToSeleccion(SeleccionEditModel seleccionEditModel)
        {
            Seleccion seleccion = new Seleccion();
            seleccion.ID = seleccionEditModel.id;
            seleccion.PaisId = seleccionEditModel.PaisId;
            seleccion.Nombre = seleccionEditModel.Nombre;
            seleccion.Email = seleccionEditModel.Email;
            seleccion.Telefono = seleccionEditModel.Telefono;
            seleccion.CantApostadores = seleccionEditModel.CantApostadores;
            seleccion.Grupo = new GrupoSeleccion() { Nombre = seleccionEditModel.Grupo };
            return seleccion;
        }
        
        public static SeleccionEditModel FromSeleccionToSeleccionEdit(Seleccion seleccion)
        {
            SeleccionEditModel edit = new SeleccionEditModel();
            edit.id = seleccion.ID;
            edit.PaisId = seleccion.PaisId;
            edit.Nombre = seleccion.Nombre;
            edit.Email = seleccion.Email;
            edit.Telefono = seleccion.Telefono;
            edit.CantApostadores = seleccion.CantApostadores;
            edit.Grupo = seleccion.Grupo.Nombre;
            return edit;
        }
    }
}
