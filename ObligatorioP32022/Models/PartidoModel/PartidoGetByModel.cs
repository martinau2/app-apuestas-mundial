﻿using System;

namespace ObligatorioP32022.Models.PartidoModel
{
    public class PartidoGetByModel
    {
        public string GrupoSeleccion { get; set; }
        public string CodigoISO { get; set; }
        public string NombreParticipante { get; set; }
        public DateTime? Fecha1 { get; set; } = null;
        public DateTime? Fecha2 { get; set; } = null;
    }
}
