﻿using LogicaDatos.Datos;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System;

namespace ObligatorioP32022.Models.PartidoModel
{
    public class PartidoShowModel
    {
        public string NombreSeleccion1 { get; set; }
        
        public int GolesSeleccion1 { get; set; }

        public int AmarillasSeleccion1 { get; set; }

        public int RojasSeleccion1 { get; set; }

        public string NombreSeleccion2 { get; set; }

        public int GolesSeleccion2 { get; set; }

        public int AmarillasSeleccion2 { get; set; }

        public int RojasSeleccion2 { get; set; }
        public DateTime FechaYHora { get; set; }
        public int PuntajeSeleccion1 { get; set; }
        public int PuntajeSeleccion2 { get; set; }
    }
}
