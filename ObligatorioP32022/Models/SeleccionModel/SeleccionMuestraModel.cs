﻿namespace ObligatorioP32022.Models.SeleccionModel
{
    public class SeleccionMuestraModel
    {
        public int id { get; set; }
        public string NombrePais { get; set; }
        public string CodigoPais { get; set; }
        public string nombreUrlBandera { get; set; }
        public string Nombre;
        public string Email { get; set; }
        public string Telefono { get; set; }
        public int CantApostadores { get; set; }
        public string Grupo { get; set; } // simplificacion del modelo
    }
}
