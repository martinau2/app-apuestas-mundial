﻿namespace ObligatorioP32022.Models.SeleccionModel
{
    public class SeleccionResultadosModel
    {
        public string NombreSeleccion { get; set; }
        public int Puntaje { get; set; }
        public int GolesAFavor { get; set; }
        public int GolesEnContra { get; set; }
        public int GolesDiferencia { get; set; }

        public SeleccionResultadosModel(string nombreSeleccion, int puntaje, int golesAFavor, int golesEnContra)
        {
            NombreSeleccion = nombreSeleccion;
            Puntaje = puntaje;
            GolesAFavor = golesAFavor;
            GolesEnContra = golesEnContra;
            GolesDiferencia = golesAFavor - golesEnContra;
        }
    }
}
