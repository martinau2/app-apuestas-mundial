﻿using LogicaDatos.Datos;

namespace ObligatorioP32022.Models.SeleccionModel
{
    public class SeleccionAltaModel
    {
        public int PaisId { get; set; }

        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public int CantApostadores { get; set; }
        public string Grupo { get; set; } // simplificacion del modelo
    }
}
