using LogicaAccesoDatos;
using LogicaAplicacion.CasosUso;
using LogicaAplicacion.CasosUso.Regiones;
using LogicaAplicacion.InterfacesCasosUso;
using LogicaAplicacion.InterfacesCasosUso.Regiones;
using LogicaAplicacion.InterfacesCasosUso.UsuarioI;
using LogicaDatos.InterfacesRepositorios;
using ManejoUsuario.BD;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObligatorioP32022
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<ProyectContext>(Options => Options.UseSqlServer(Configuration.GetConnectionString("ConexionLocalDB")));
            services.AddDbContext<UserContext>(Options => Options.UseSqlServer(Configuration.GetConnectionString("ConexionUserDB")));
            services.AddScoped<IRepositorioPais, RepositorioPais>();
            services.AddScoped<IRepositorioRegion, RepositorioRegion>();
            services.AddScoped<IRepositorioUsuario, RepositorioUsuario>();
            services.AddScoped<IPaisCasoUso, PaisCasoUso>();
            services.AddScoped<IRegionCasoUso, RegionCasoUso>();
            services.AddScoped<IUsuarioCasoUso, UsuarioCasoUso>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
