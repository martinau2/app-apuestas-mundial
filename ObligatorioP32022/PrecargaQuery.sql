﻿
insert into Regiones (Nombre) values ('America');
insert into Regiones (Nombre) values ('Asia');
insert into Regiones (Nombre) values ('Europa');
insert into Regiones (Nombre) values ('Africa');
insert into Regiones (Nombre) values ('Oceania');


insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Argentina', 'ARG',56000000,3000000,1,'ARG.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Australia', 'AUS',123000000,45000000,5,'AUS.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Brasil', 'BRA',567000000,67000000,1,'BRA.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Canada', 'CAN',256000000,34000000,1,'CAN.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('China', 'CHN',900000000,2230000000,2,'CHN.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Egipto', 'EGY',70000000,24000000,4,'EGY.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Francia', 'FRA',500000000,34000000,3,'FRA.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('India', 'IND',203000000,23000000,2,'IND.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Japon', 'JPN',182000000,52000000,2,'JPN.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Mexico', 'MEX',46000000,42000000,1,'MEX.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Rusia', 'RUS',23000000,15000000,3,'RUS.png');
insert into Paises (nombre, codigo,PBI,Poblacion,RegionID,Imagen) values ('Sudafrica', 'ZAF',50000000,15000000,4,'ZAF.png');

insert into Selecciones(PaisId,Nombre,Email,Telefono,CantApostadores,Grupo) values (1,'Argentina','argentina@gmail.com.gub',0931456721,10200,'B');
insert into Selecciones(PaisId,Nombre,Email,Telefono,CantApostadores,Grupo) values (2,'Australia','australia@gmail.com.gub',0958456721,10030,'B');
insert into Selecciones(PaisId,Nombre,Email,Telefono,CantApostadores,Grupo) values (3,'Brasil','brasil@gmail.com.gub',0921456721,10000,'A');
insert into Selecciones(PaisId,Nombre,Email,Telefono,CantApostadores,Grupo) values (4,'Canada','canada@gmail.com.gub',0928456721,10000,'A');
insert into Selecciones(PaisId,Nombre,Email,Telefono,CantApostadores,Grupo) values (5,'China','china@gmail.com.gub',0922476721,10000,'A');
insert into Selecciones(PaisId,Nombre,Email,Telefono,CantApostadores,Grupo) values (6,'Egipto','egipto@gmail.com.gub',0922556721,10000,'A');

insert into Fases(Discriminator) values ('Grupo')


insert into Partidos(Seleccion1ID,Seleccion2ID,FechaYHora,FaseID) values (3,4,'2022-11-25T16:00:00',1)
insert into Partidos(Seleccion1ID,Seleccion2ID,FechaYHora,FaseID) values (3,5,'2022-11-25T13:00:00',1)
insert into Partidos(Seleccion1ID,Seleccion2ID,FechaYHora,FaseID) values (3,6,'2022-11-25T7:00:00',1)
insert into Partidos(Seleccion1ID,Seleccion2ID,FechaYHora,FaseID) values (5,6,'2022-11-24T13:00:00',1)
insert into Partidos(Seleccion1ID,Seleccion2ID,FechaYHora,FaseID) values (6,4,'2022-11-24T07:00:00',1)


UPDATE Partidos SET Goles_Seleccion1 = 2, Amarillas_Seleccion1 = 1, Roja_Seleccion1 = 1, Goles_Seleccion2 = 1, Amarillas_Seleccion2 = 0, Roja_Seleccion2 = 0, PuntajeSeleccion1 = 3, PuntajeSeleccion2 = 0 WHERE ID = 1;
UPDATE Partidos SET Goles_Seleccion1 = 2, Amarillas_Seleccion1 = 1, Roja_Seleccion1 = 1, Goles_Seleccion2 = 2, Amarillas_Seleccion2 = 0, Roja_Seleccion2 = 0, PuntajeSeleccion1 = 1, PuntajeSeleccion2 = 1 WHERE ID = 2;
UPDATE Partidos SET Goles_Seleccion1 = 2, Amarillas_Seleccion1 = 1, Roja_Seleccion1 = 1, Goles_Seleccion2 = 4, Amarillas_Seleccion2 = 0, Roja_Seleccion2 = 0, PuntajeSeleccion1 = 0, PuntajeSeleccion2 = 3 WHERE ID = 3;
UPDATE Partidos SET Goles_Seleccion1 = 3, Amarillas_Seleccion1 = 1, Roja_Seleccion1 = 1, Goles_Seleccion2 = 1, Amarillas_Seleccion2 = 0, Roja_Seleccion2 = 0, PuntajeSeleccion1 = 3, PuntajeSeleccion2 = 0 WHERE ID = 4;
