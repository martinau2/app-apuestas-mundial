﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Text;

namespace LogicaDatos.Datos
{
    public class Partido
    {
        public int ID { get; set; }

        [ForeignKey("Seleccion1")]
        public int Seleccion1ID { get; set; }
        public Seleccion Seleccion1 { get; set; }

        public ResultadoPartido Resultado1 { get; set; }

        [ForeignKey("Seleccion2")]
        public int Seleccion2ID { get; set; }
        public Seleccion Seleccion2 { get; set; }

        [DataType(DataType.Date)]
        public DateTime FechaYHora { get; set; }

        public ResultadoPartido Resultado2 { get; set; }

        [ForeignKey("Fase")]
        public int FaseID { get; set; }

        public int PuntajeSeleccion1 { get; set; }
        public int PuntajeSeleccion2 { get; set; }
        
        public bool Validar()
        {
            bool valido = true;
            if (Seleccion1ID == Seleccion2ID)
            {
                valido = false;
            }
            return valido;
        }

        public void CalcularPuntajePartido()
        {
            {
                if (Resultado1.GolesSeleccion > Resultado2.GolesSeleccion )
                {
                    PuntajeSeleccion1 = 3;
                    PuntajeSeleccion2 = 0;
                }
                else if (Resultado1.GolesSeleccion == Resultado2.GolesSeleccion)
                {
                    PuntajeSeleccion1 = 1;
                    PuntajeSeleccion2 = 1;
                }
                else
                {
                    PuntajeSeleccion1 = 0;
                    PuntajeSeleccion2 = 3;
                }
            }
        }
    }
}
