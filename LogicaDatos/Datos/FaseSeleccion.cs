﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LogicaDatos.Datos
{
    public class FaseSeleccion
    {
        [ForeignKey("Fase")]
        public int FaseID { get; set; }

        public Fase Fase { get; set; }

        [ForeignKey("Seleccion")]
        public int SeleccionID { get; set; }

        public Seleccion Seleccion { get; set; }
    }
}
