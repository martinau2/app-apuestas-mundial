﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.Datos
{
    public abstract class Fase
    {
        public int ID { get; set; }
        
        public ICollection<FaseSeleccion> Selecciones { get; set; }

        public ICollection<Partido> Partidos { get; set; }
    }
}
