﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.Datos
{
    public class Eliminatoria : Fase
    {
        public int GanadorID { get; set; }
        public Seleccion Ganador { get; set; }
    }
}
