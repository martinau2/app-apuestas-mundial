﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicaDatos.Datos
{
    //value object
    public class GrupoSeleccion : IComparable
    {
        public string Nombre { get; set; }

        public int CompareTo(object obj)
        {
            if (typeof(GrupoSeleccion) == obj.GetType())
            {
                if (this.Nombre == ((GrupoSeleccion)obj).Nombre)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return -1;
            }
        }

        public bool ValidarGrupo()
        {
            var ListOfGroups = new string[8] { "A", "B", "C", "D", "E", "F", "G", "H" };
            //is Nombre any of listOfGroups
            foreach(var group in ListOfGroups)
            {
                if (Nombre == group)
                    return true;
            }
            return false;
        }
    }
}
