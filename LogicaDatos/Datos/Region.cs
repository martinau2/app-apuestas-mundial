﻿using System.ComponentModel.DataAnnotations;

namespace LogicaDatos.Datos
{
    public class Region
    {

        [Key]
        public int ID { get; set; }
        [Required]
        [StringLength(20)]
        public string Nombre { get; set; }
    }
}