﻿using SistemaMundial.LogicaNegocio.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.RegularExpressions;

namespace LogicaDatos.Datos
{
    public class Pais : IValidable<Pais>
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [StringLength(20)]


        public string Nombre { get; set; }
        public string Codigo { get; set; }
        public double PBI { get; set; }
        public double Poblacion { get; set; }
        public string Imagen { get; set; }

        [ForeignKey("_Region")]
        public int RegionID { get; set; }
        public Region _Region{ get; set; }

    public bool ValidarNombre() 
        {
            bool valido = false;
            //Utilizo una expresion regular que me compara una string
            //a un patron y ve si coincide

            if (Regex.IsMatch(Nombre, @"^[a-zA-Z\s]+$"))
                valido = true;
            return valido;

        }
        public bool ValidarCodigo() 
        {
            bool valido = false;
            if (Codigo.Length == 3 && Codigo.StartsWith(Nombre.Substring(0, 1)))
            {
                if (Regex.IsMatch(Codigo, @"^[A-Z]+$"))
                {
                    valido = true;
                }
            }
            return valido;
        }
        public bool ValidarPBI() 
        {
            bool valido = false;
            if (PBI > 0)
                valido = true;
            return valido;
        }
        public bool ValidarPoblacion() 
        {
            bool valido = false;
            if (Poblacion > 0)
                valido = true;
            return valido;
        }

        //function validar for all above
        public bool Validar()
        {
            bool valido = false;
            bool nombre = ValidarNombre();
            bool codigo = ValidarCodigo();
            bool pbi = ValidarPBI();
            bool poblacion = ValidarPoblacion();
            if (nombre && codigo && pbi && poblacion)
                valido = true;
            return valido;
        }


    }
}
