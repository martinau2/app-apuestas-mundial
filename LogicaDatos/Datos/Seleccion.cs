﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LogicaDatos.Datos
{
    public class Seleccion
    {
        [Key]
        public int ID { get; set; }
        [ForeignKey("Pais")]
        public int PaisId { get; set; }
        public Pais _Pais { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public int CantApostadores { get; set; }
        public GrupoSeleccion Grupo { get; set; }

        public bool Validar()
        {
            if (ValidarNombre() && ValidarEmail() && ValidarTelefono() && ValidarNumerosPositivos() && Grupo.ValidarGrupo())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        
        public bool ValidarNombre()
        {
            if (Nombre != null)
            {
                foreach (char c in Nombre)
                {
                    if (!char.IsLetter(c) && c != ' ')
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public bool ValidarEmail()
        {
            if (Email != null)
            {
                if (Email.Contains("@") && Email.Contains("."))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        
        public bool ValidarTelefono()
        {
            if (Telefono.Length < 7)
            {
                return false;
            }
            else
            {
                foreach (var c in Telefono)
                {
                    if (!char.IsNumber(c))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public bool ValidarNumerosPositivos()
        {
            {
                if (CantApostadores < 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}
