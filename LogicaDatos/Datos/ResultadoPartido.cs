﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.Datos
{
    public class ResultadoPartido
    {
        public int GolesSeleccion { get; set; }
        public int CantAmarillas { get; set; }
        public int CantRojas { get; set; }

        public void CalcularRojas()
        {
            CantRojas = CantAmarillas / 2;
        }
    }
}
