﻿using SistemaMundial.LogicaNegocio.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.Datos
{
    public class Grupo : Fase
    {
        

        public List<Seleccion> SeleccionesDeGrupo(GrupoSeleccion grupo)
        {
            {
                List<Seleccion> seleccionesEnGrupo = new List<Seleccion>();
                foreach (var seleccion in Selecciones)
                {
                    if (seleccion.Seleccion.Grupo == grupo)
                    {
                        seleccionesEnGrupo.Add(seleccion.Seleccion);
                    }
                }
                return seleccionesEnGrupo;
            }
        }
    }
}
