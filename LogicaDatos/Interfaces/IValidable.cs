﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaMundial.LogicaNegocio.Interfaces
{
    public interface IValidable<T> where T: class
    {
        public bool Validar();
    }
}
