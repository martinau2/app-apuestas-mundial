﻿using Dominio.InterfacesRepositorios;
using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.InterfacesRepositorios
{
    public interface IRepositorioGrupo : IRepositorio<Grupo>
    {
        public bool AddPartido(Partido obj,int? idGrupo);
        public bool RemovePartido(int id);
        public bool UpdatePartido(Partido obj);
        public IEnumerable<Partido> FindAllPartido(int? idGrupo);
        public Partido FindByIdPartido(int? id);

        public Partido PartidoPorSeleccion(int? idSeleccion);

        public IEnumerable<Partido> PartidosPorGrupo(string? nombreGrupo);

        public Partido PartidoPorSelecciones(int? idSeleccion1, int? idSeleccion2);

        public Partido PartidoPorFecha(DateTime fecha);
    }
}
