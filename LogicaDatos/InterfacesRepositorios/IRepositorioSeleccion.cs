﻿using Dominio.InterfacesRepositorios;
using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.InterfacesRepositorios
{
    public interface IRepositorioSeleccion : IRepositorio<Seleccion>
    {
        IEnumerable<Seleccion> SeleccionesDeGrupo(GrupoSeleccion grupo);

        Seleccion SeleccionPorPais(int? id);

        public int PuntajePorSeleccion(int? idSeleccion);
        public int GolesAFavorPorSeleccion(int? idSeleccion);
        public int GolesAEncontraPorSeleccion(int? idSeleccion);
    }
}
