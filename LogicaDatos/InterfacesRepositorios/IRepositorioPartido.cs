﻿using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.InterfacesRepositorios
{
    public interface IRepositorioPartido
    {
        public bool AddResultadoPartido(int? idPartido, ResultadoPartido resultado1, ResultadoPartido resultado2);

        public IEnumerable<Partido> GetPartidoBy(GrupoSeleccion grupoSeleccion, string codigoISO,string nombreParticipante,DateTime? fecha1, DateTime? fecha2);
    }
}
