﻿using Dominio.InterfacesRepositorios;
using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.InterfacesRepositorios
{
    public interface IRepositorioRegion : IRepositorio<Region>
    {
    }
}
