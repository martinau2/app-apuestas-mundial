﻿using Dominio.InterfacesRepositorios;
using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.InterfacesRepositorios
{
    public interface IRepositorioPais : IRepositorio<Pais>
    {
        public IEnumerable<Pais> FindForSearch(int? id, string codigo, int? region);
    }
}
