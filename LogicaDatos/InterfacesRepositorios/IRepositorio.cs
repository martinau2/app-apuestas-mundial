﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dominio.InterfacesRepositorios
{
    public interface IRepositorio<T> where T : class
    {
        public bool Add(T obj);
        public bool Remove(int id);
        public bool Update(T obj);
        public IEnumerable<T> FindAll();
        public T FindById(int id);
    }
}
