﻿using LogicaDatos.InterfacesRepositorios;
using ManejoUsuario;
using ManejoUsuario.BD;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicaAccesoDatos
{
    public class RepositorioUsuario : IRepositorioUsuario
    {
        private UserContext db { get; set; }

        public RepositorioUsuario(UserContext db)
        {
            this.db = db;
        }

        public bool Add(Usuario obj)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Usuario obj)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Usuario> FindAll()
        {
            try
            {
                return db.Usuarios.Include(u => u._Rol);


            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar los paises", ex);
            }
        }

        public Usuario FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Usuario FindByEmail(string email)
        {
            try
            {
                return db.Usuarios.Include(u => u._Rol).FirstOrDefault(u => u.Email == email);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el usuario", ex);
            }
        }

        public Usuario FindByUsername(string username)
        {
            throw new NotImplementedException();
        }

        public Usuario Login(string Email, string Password)
        {
            try
            {
                var usuario = db.Usuarios.Include(u => u._Rol).FirstOrDefault(u => u.Email == Email && u.Password == Password);
                if (usuario == null)
                {
                    return null;
                }
                else
                {
                    return usuario;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el usuario", ex);
            }
        
        }

        public Usuario Register(Usuario usuario)
        {
            if (usuario == null || !usuario.Validar())
            {
                return null;
            }
            else
            {
                usuario.RolID = 3;
                db.Usuarios.Add(usuario);
                db.SaveChanges();
                return usuario;
            }
        }

    }
}
