﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LogicaAccesoDatos.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Regiones",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regiones", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Paises",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(maxLength: 20, nullable: false),
                    Codigo = table.Column<string>(nullable: false),
                    PBI = table.Column<double>(nullable: false),
                    Poblacion = table.Column<double>(nullable: false),
                    Imagen = table.Column<string>(nullable: false),
                    RegionID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Paises", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Paises_Regiones_RegionID",
                        column: x => x.RegionID,
                        principalTable: "Regiones",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Selecciones",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PaisId = table.Column<int>(nullable: false),
                    Nombre = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Telefono = table.Column<string>(nullable: false),
                    CantApostadores = table.Column<int>(nullable: false),
                    Grupo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Selecciones", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Selecciones_Paises_PaisId",
                        column: x => x.PaisId,
                        principalTable: "Paises",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Fases",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Discriminator = table.Column<string>(nullable: false),
                    GanadorID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fases", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Fases_Selecciones_GanadorID",
                        column: x => x.GanadorID,
                        principalTable: "Selecciones",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "FaseSelecciones",
                columns: table => new
                {
                    FaseID = table.Column<int>(nullable: false),
                    SeleccionID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FaseSelecciones", x => new { x.SeleccionID, x.FaseID });
                    table.ForeignKey(
                        name: "FK_FaseSelecciones_Fases_FaseID",
                        column: x => x.FaseID,
                        principalTable: "Fases",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_FaseSelecciones_Selecciones_SeleccionID",
                        column: x => x.SeleccionID,
                        principalTable: "Selecciones",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Partidos",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Seleccion1ID = table.Column<int>(nullable: false),
                    Goles_Seleccion1 = table.Column<int>(nullable: true),
                    Amarillas_Seleccion1 = table.Column<int>(nullable: true),
                    Roja_Seleccion1 = table.Column<int>(nullable: true),
                    Seleccion2ID = table.Column<int>(nullable: false),
                    FechaYHora = table.Column<DateTime>(nullable: false),
                    Goles_Seleccion2 = table.Column<int>(nullable: true),
                    Amarillas_Seleccion2 = table.Column<int>(nullable: true),
                    Roja_Seleccion2 = table.Column<int>(nullable: true),
                    FaseID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partidos", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Partidos_Fases_FaseID",
                        column: x => x.FaseID,
                        principalTable: "Fases",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Partidos_Selecciones_Seleccion1ID",
                        column: x => x.Seleccion1ID,
                        principalTable: "Selecciones",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Partidos_Selecciones_Seleccion2ID",
                        column: x => x.Seleccion2ID,
                        principalTable: "Selecciones",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Fases_GanadorID",
                table: "Fases",
                column: "GanadorID");

            migrationBuilder.CreateIndex(
                name: "IX_FaseSelecciones_FaseID",
                table: "FaseSelecciones",
                column: "FaseID");

            migrationBuilder.CreateIndex(
                name: "IX_Paises_Codigo",
                table: "Paises",
                column: "Codigo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Paises_Nombre",
                table: "Paises",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Paises_RegionID",
                table: "Paises",
                column: "RegionID");

            migrationBuilder.CreateIndex(
                name: "IX_Partidos_FaseID",
                table: "Partidos",
                column: "FaseID");

            migrationBuilder.CreateIndex(
                name: "IX_Partidos_Seleccion1ID",
                table: "Partidos",
                column: "Seleccion1ID");

            migrationBuilder.CreateIndex(
                name: "IX_Partidos_Seleccion2ID",
                table: "Partidos",
                column: "Seleccion2ID");

            migrationBuilder.CreateIndex(
                name: "IX_Regiones_Nombre",
                table: "Regiones",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Selecciones_Nombre",
                table: "Selecciones",
                column: "Nombre",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Selecciones_PaisId",
                table: "Selecciones",
                column: "PaisId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FaseSelecciones");

            migrationBuilder.DropTable(
                name: "Partidos");

            migrationBuilder.DropTable(
                name: "Fases");

            migrationBuilder.DropTable(
                name: "Selecciones");

            migrationBuilder.DropTable(
                name: "Paises");

            migrationBuilder.DropTable(
                name: "Regiones");
        }
    }
}
