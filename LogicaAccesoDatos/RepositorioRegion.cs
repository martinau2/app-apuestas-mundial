﻿using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicaAccesoDatos
{
    public class RepositorioRegion : IRepositorioRegion
    {
        private ProyectContext db { get; set; }

        public RepositorioRegion(ProyectContext db)
        {
            this.db = db;
        }
        public bool Add(Region obj)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Region> FindAll()
        {
            try
            {
                return db.Regiones;
                

            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar los paises", ex);
            }
        }

        public Region FindById(int id)
        {
            var regiones = db.Regiones;
            Region region = null;
            foreach (var reg in regiones)
            {
                if (reg.ID == id)
                {
                    region = reg;
                }
            }

            if (region == null)
            {
                throw new Exception("No se encontro la region");
            }
            return region;
        }
        
        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public bool Update(Region obj)
        {
            throw new NotImplementedException();
        }
    }
}
