﻿using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;
using LogicaDatos.InterfacesRepositorios;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace LogicaAccesoDatos
{
    public class RepositorioPais : IRepositorioPais
    {
        private ProyectContext db { get; set; }

        public RepositorioPais(ProyectContext db)
        {
            this.db = db;
        }
        public bool Add(Pais obj)
        {

            if (obj == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            if (obj.Validar())
            {
                try
                {
                    
                        
                    db.Paises.Add(obj);
                    db.SaveChanges();

                    return true;


                }
                catch(Exception ex){
                    return false;
                    throw new Exception("Error al agregar el pais", ex);
                }
            }
            else
            {
                return false;
                throw new Exception("Error al agregar el pais");
            }
        }

        public IEnumerable<Pais> FindAll()
        {
            try
            {
                
                return db.Paises.Include(pais => pais._Region);
                
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar los paises", ex);
            }
        }

        public Pais FindById(int id)
        {
            try
            {

                var pais = db.Paises.Include(pais => pais._Region).Where(paises => paises.ID == id).FirstOrDefault();
                return pais;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el pais", ex);
            }
        }

        public bool Remove(int id)
        {
            bool relatedRegistry = false;
            var selecciones = db.Selecciones;
            //find seleccion with id of pais
            foreach (var seleccion in selecciones)
            {
                if (seleccion.PaisId == id)
                {
                    relatedRegistry = true;
                    break;
                }
            }

            if (relatedRegistry)
            {
                return false;
                throw new Exception("No se puede eliminar el pais porque tiene selecciones asociadas");
            }
            else
            {
                try
                {
                    var pais = db.Paises.Find(id);
                    db.Paises.Remove(pais);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                    throw new Exception("Error al eliminar el pais", ex);
                }
            }
        }

        public bool Update(Pais obj)
        {
            if (obj == null)
            {
                return false;
                throw new ArgumentNullException("No se puede actualizar un objeto nulo");
            }
            if (obj.Validar())
            {
                try
                {
                    var pais = db.Paises.Find(obj.ID);
                    pais.Nombre = obj.Nombre;
                    pais.Codigo = obj.Codigo;
                    pais.Poblacion = obj.Poblacion;
                    pais.PBI = obj.PBI;
                    pais.Imagen = obj.Imagen;
                    pais.RegionID = obj.RegionID;
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                    throw new Exception("Error al actualizar el pais", ex);
                }
            }
            else
            {
                return false;
                throw new Exception("Error al validar el pais");
            }
        }

        public IEnumerable<Pais> FindForSearch(int? id, string codigo, int? region)
        {
            try
            {
                var paises = db.Paises.Include(pais => pais._Region).AsQueryable();
                if (id != null)
                {
                    paises = paises.Where(pais => pais.ID == id);
                }
                if (codigo != null)
                {
                    paises = paises.Where(pais => pais.Codigo == codigo);
                }
                if (region != null)
                {
                    paises = paises.Where(pais => pais.RegionID == region);
                }
                return paises;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar los paises", ex);
            }
        }
    }
}
