﻿using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicaAccesoDatos
{
    public class RepositorioPartido : IRepositorioPartido
    {
        private ProyectContext db { get; set; }

        public RepositorioPartido(ProyectContext db)
        {
            this.db = db;
        }
        
        public bool AddResultadoPartido(int? idPartido, ResultadoPartido resultado1, ResultadoPartido resultado2)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Partido> GetPartidoBy(GrupoSeleccion grupoSeleccion, string codigoISO, string nombreParticipante, DateTime? fecha1, DateTime? fecha2)
        {
            try
            {
                var partidos = db.Partidos.Include(p => p.Seleccion1).ThenInclude(s => s._Pais).Include(p => p.Seleccion2).ThenInclude(s => s._Pais).AsQueryable();
                if (grupoSeleccion != null)
                {
                    partidos = partidos.Where(p => p.Seleccion1.Grupo.Nombre == grupoSeleccion.Nombre);
                }
                if (codigoISO != null)
                {
                    partidos = partidos.Where(p => p.Seleccion1._Pais.Codigo == codigoISO || p.Seleccion2._Pais.Codigo == codigoISO);
                }
                if (nombreParticipante != null)
                {
                    partidos = partidos.Where(p => p.Seleccion1.Nombre == nombreParticipante || p.Seleccion2.Nombre == nombreParticipante || p.Seleccion1._Pais.Nombre == nombreParticipante || p.Seleccion2._Pais.Nombre == nombreParticipante);
                }
                if(fecha1 != null && fecha2 != null)
                {
                    partidos = partidos.Where(p => p.FechaYHora > fecha1 && p.FechaYHora < fecha2);
                }
                return partidos.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar los partidos", ex);
            }
        }
    }
}
