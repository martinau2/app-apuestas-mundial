﻿using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicaAccesoDatos
{
    public class RepositorioSeleccion : IRepositorioSeleccion
    {

         private ProyectContext db { get; set; }

        public RepositorioSeleccion(ProyectContext db)
        {
            this.db = db;
        }

        public bool Add(Seleccion obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }

            if (SeleccionPorPais(obj.PaisId) != null)
            {
                throw new ArgumentException("Ya existe una seleccion con ese pais");
            }

            var cantidadDeGrupo = SeleccionesDeGrupo(obj.Grupo).Count();
            if (cantidadDeGrupo == 4)
            {
                throw new ArgumentException("Ya hay 4 selecciones en ese grupo");
            }

            if (obj.Validar() && obj.Grupo.ValidarGrupo())
            {
                try
                {
                    db.Selecciones.Add(obj);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                    throw new Exception("Error al agregar la seleccion", ex);
                }
            }
            else
            {
                return false;
            }
            
            
        }
    

        public IEnumerable<Seleccion> FindAll()
        {
            try
            {

                return db.Selecciones.Include(seleccion => seleccion._Pais);

            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar las selecciones", ex);
            }
        }

        public Seleccion FindById(int id)
        {
            try
            {
                return db.Selecciones.Where(seleccion => seleccion.ID == id).Include(seleccion => seleccion._Pais).ThenInclude(pais => pais._Region).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar la seleccion", ex);
            }
        }
        

        public bool Remove(int id)
        {
            try
            {
                Seleccion seleccion = FindById(id);
                if (seleccion != null)
                {
                    db.Selecciones.Remove(seleccion);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Error al eliminar la seleccion", ex);
            }
        }
    

        public IEnumerable<Seleccion> SeleccionesDeGrupo(GrupoSeleccion grupo)
        {
            try
            {
                return db.Selecciones.Include(seleccion => seleccion._Pais).Where(seleccion => seleccion.Grupo.Nombre == grupo.Nombre);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar las selecciones del grupo", ex);
            }
        }
    

        public bool Update(Seleccion obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("No se puede actualizar un objeto nulo");
            }

            if (obj.Validar() && obj.Grupo.ValidarGrupo())
            {
                try
                {
                    db.Selecciones.Update(obj);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                    throw new Exception("Error al actualizar la seleccion", ex);
                }
            }
            else
            {
                return false;
                throw new Exception("Error al validar la seleccion");
            }
        }



        public Seleccion SeleccionPorPais(int? id)
        {
            if (id == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            
            try
            {
                return db.Selecciones.Include(seleccion => seleccion._Pais).Where(seleccion => seleccion.PaisId == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar la seleccion", ex);
            }
        }

        public int PuntajePorSeleccion(int? idSeleccion)
        {
            if (idSeleccion == null)
            {
                throw new ArgumentNullException("No se puede buscar un partido con una selección nula");
            }
            try
            {
                var partidos = db.Partidos.Where(p => p.Seleccion1ID == idSeleccion || p.Seleccion2ID == idSeleccion).ToList();
                int puntaje = 0;
                foreach (var partido in partidos)
                {
                    if (partido.Seleccion1ID == idSeleccion)
                    {
                        puntaje += partido.PuntajeSeleccion1;
                    }
                    else
                    {
                        puntaje += partido.PuntajeSeleccion2;
                    }
                }

                return puntaje;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }

        public int GolesAFavorPorSeleccion(int? idSeleccion)
        {
            if (idSeleccion == null)
            {
                throw new ArgumentNullException("No se puede buscar un partido con una selección nula");
            }
            try
            {
                var partidos = db.Partidos.Where(p => p.Seleccion1ID == idSeleccion || p.Seleccion2ID == idSeleccion).ToList();
                int goles = 0;
                foreach (var partido in partidos)
                {
                    if(partido.Resultado1 != null && partido.Resultado2 != null)
                    {
                        if (partido.Seleccion1ID == idSeleccion)
                        {
                            goles += partido.Resultado1.GolesSeleccion;
                        }
                        else
                        {
                            goles += partido.Resultado2.GolesSeleccion;
                        }
                    }
                }
                return goles;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }

        public int GolesAEncontraPorSeleccion(int? idSeleccion)
        {
            if (idSeleccion == null)
            {
                throw new ArgumentNullException("No se puede buscar un partido con una selección nula");
            }
            try
            {
                var partidos = db.Partidos.Where(p => p.Seleccion1ID == idSeleccion || p.Seleccion2ID == idSeleccion).ToList();
                int goles = 0;
                foreach (var partido in partidos)
                {
                    if (partido.Resultado1 != null && partido.Resultado2 != null)
                    {
                        if (partido.Seleccion1ID != idSeleccion)
                        {
                            goles += partido.Resultado1.GolesSeleccion;
                        }
                        else
                        {
                            goles += partido.Resultado2.GolesSeleccion;
                        }
                    }
                }
                return goles;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }
    }
}
