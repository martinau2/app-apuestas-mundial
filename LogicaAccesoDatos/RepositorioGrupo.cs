﻿using Dominio.InterfacesRepositorios;
using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicaAccesoDatos
{
    public class RepositorioGrupo : IRepositorioGrupo , IRepositorioPartido
    {
        private ProyectContext db { get; set; }

        public RepositorioGrupo(ProyectContext db)
        {
            this.db = db;
        }

        public bool Add(Grupo obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            try
            {
                db.FasesGrupos.Add(obj);
                db.SaveChanges();

                return true;


            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Error al agregar la fase de grupos", ex);
            }
        }


        public IEnumerable<Grupo> FindAll()
        {
            try
            {
                return db.FasesGrupos.Include(grupo => grupo.Partidos);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar las fases de grupos", ex);
            }

        }

        public Grupo FindById(int id)
        {
            try
            {
                return db.FasesGrupos.Include(grupo => grupo.Selecciones).Include(grupo => grupo.Partidos).FirstOrDefault(grupo => grupo.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar la fase de grupos", ex);
            }
        }

        public bool Remove(int id)
        {
            try
            {
                var grupo = db.FasesGrupos.FirstOrDefault(grupo => grupo.ID == id);
                if (grupo.Partidos != null && grupo.Partidos.Count() > 0)
                {
                    throw new Exception("No se puede eliminar la fase de grupos porque tiene partidos asociados");
                }
                if (grupo != null)
                {
                    db.FasesGrupos.Remove(grupo);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al eliminar la fase de grupos", ex);
            }
        }


        public bool Update(Grupo obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("No se puede actualizar un objeto nulo");
            }
            try
            {
                db.FasesGrupos.Update(obj);
                db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Error al actualizar la fase de grupos", ex);
            }
        }

        public bool AddPartido(Partido obj, int? idGrupo)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            if (!obj.Validar())
            {
                throw new Exception("Error al validar el partido");
            }
            if (obj.FechaYHora < new DateTime(2022, 11, 20) && obj.FechaYHora > new DateTime(2022, 12, 02))
            {
                throw new Exception("Error de fecha del partido");
            }
            if (obj.FechaYHora.Hour != 7 && obj.FechaYHora.Hour != 10 && obj.FechaYHora.Hour != 13 && obj.FechaYHora.Hour != 16)
            {
                throw new Exception("Horario no valido");
            }

            var partidoYaJugado = PartidoPorSelecciones(obj.Seleccion1ID, obj.Seleccion2ID);
            if (partidoYaJugado != null)
            {
                throw new Exception("Error al agregar el partido, ya existe un partido entre estas selecciones");
            }

            var partidoEnEsaHora = PartidoPorFecha(obj.FechaYHora);
            if (partidoEnEsaHora != null)
            {
                throw new Exception("Error al agregar el partido, ya existe un partido en esta fecha y hora");
            }

            var seleccion1 = db.Selecciones.FirstOrDefault(seleccion => seleccion.ID == obj.Seleccion1ID);
            var seleccion2 = db.Selecciones.FirstOrDefault(seleccion => seleccion.ID == obj.Seleccion2ID);
            if(seleccion1 == null || seleccion2 == null)
            {
                throw new Exception("Error una de las selecciones es null");
            }
            if (seleccion1.Grupo.CompareTo(seleccion2.Grupo) != 0)
            {
                throw new Exception("Error al agregar el partido, ya que las selecciones son de grupos distintos");
            }

            try
            {
                obj.FaseID = (int)idGrupo;
                db.Partidos.Add(obj);
                db.SaveChanges();

                return true;


            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Error al agregar la fase de grupos", ex);
            }
        }

        public bool UpdatePartido(Partido obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            if (!obj.Validar())
            {
                throw new Exception("Error al validar el partido");
            }
            if (obj.FechaYHora < new DateTime(2022, 11, 20) && obj.FechaYHora > new DateTime(2022, 12, 02))
            {
                throw new Exception("Error de fecha del partido");
            }
            if (obj.FechaYHora.Hour != 7 && obj.FechaYHora.Hour != 10 && obj.FechaYHora.Hour != 13 && obj.FechaYHora.Hour != 16)
            {
                throw new Exception("Horario no valido");
            }

            var partidoYaJugado = PartidoPorSelecciones(obj.Seleccion1ID, obj.Seleccion2ID);
            if (partidoYaJugado != null && partidoYaJugado.ID != obj.ID)
            {
                throw new Exception("Error al agregar el partido, ya existe un partido entre estas selecciones");
            }

            var partidoEnEsaHora = PartidoPorFecha(obj.FechaYHora);
            if (partidoEnEsaHora != null && partidoYaJugado.ID != obj.ID)
            {
                throw new Exception("Error al agregar el partido, ya existe un partido en esta fecha y hora");
            }

            var seleccion1 = db.Selecciones.FirstOrDefault(seleccion => seleccion.ID == obj.Seleccion1ID);
            var seleccion2 = db.Selecciones.FirstOrDefault(seleccion => seleccion.ID == obj.Seleccion2ID);
            if (seleccion1 == null || seleccion2 == null)
            {
                throw new Exception("Error una de las selecciones es null");
            }
            if (seleccion1.Grupo.CompareTo(seleccion2.Grupo) != 0)
            {
                throw new Exception("Error al agregar el partido, ya que las selecciones son de grupos distintos");
            }

            try
            {
                db.Partidos.Update(obj);
                db.SaveChanges();

                return true;


            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Error al agregar la fase de grupos", ex);
            }
        }

        public IEnumerable<Partido> FindAllPartido(int? idGrupo)
        {
            try
            {
                return db.Partidos.Where(p => p.FaseID == idGrupo);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar los partidos", ex);
            }
        }
    

        public Partido FindByIdPartido(int? id)
        {
            try
            {
                return db.Partidos.Include(p => p.Seleccion1).Include(p => p.Seleccion2).FirstOrDefault(p => p.ID == id);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }
    

        public Partido PartidoPorSeleccion(int? idSeleccion)
        {
            if (idSeleccion == null)
            {
                throw new ArgumentNullException("No se puede buscar un partido con una selección nula");
            }
            try
            {
                return db.Partidos.Include(partido => partido.Seleccion1).Include(partido => partido.Seleccion2).FirstOrDefault(partido => partido.Seleccion1ID == idSeleccion || partido.Seleccion2ID == idSeleccion);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }

        public Partido PartidoPorSelecciones(int? idSeleccion1, int? idSeleccion2)
        {
            if (idSeleccion1 == null || idSeleccion2 == null)
            {
                throw new ArgumentNullException("No se puede buscar un partido con una selección nula");
            }
            try
            {
                return db.Partidos.Include(partido => partido.Seleccion1).Include(partido => partido.Seleccion2).FirstOrDefault(partido => (partido.Seleccion1ID == idSeleccion1 || partido.Seleccion2ID == idSeleccion1) && (partido.Seleccion1ID == idSeleccion2 || partido.Seleccion2ID == idSeleccion2));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }

        public Partido PartidoPorFecha(DateTime fecha)
        {
            try
            {
                return db.Partidos.Include(partido => partido.Seleccion1).Include(partido => partido.Seleccion2).Where(partido => partido.FechaYHora.Date == fecha.Date).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }

        public bool RemovePartido(int id)
        {
            try
            {
                var obj = FindByIdPartido(id);
                if (obj == null)
                {
                    throw new Exception("Error al buscar el partido");
                }
                db.Partidos.Remove(obj);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Error al eliminar el partido", ex);
            }
        }

        public bool AddResultadoPartido(int? idPartido, ResultadoPartido resultado1, ResultadoPartido resultado2)
        {
            if (idPartido == null)
            {
                throw new ArgumentNullException("No se puede agregar un resultado a un partido nulo");
            }
            if (resultado1 == null || resultado2 == null)
            {
                throw new ArgumentNullException("No se puede agregar un resultado nulo");
            }
            try
            {
                var partido = FindByIdPartido(idPartido);
                if (partido == null)
                {
                    throw new Exception("Error al buscar el partido");
                }
                partido.Resultado1 = resultado1;
                partido.Resultado2 = resultado2;
                db.Partidos.Update(partido);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Error al agregar el resultado del partido", ex);
            }
        }

        public IEnumerable<Partido> PartidosPorGrupo(string? nombreGrupo)
        {
            if (nombreGrupo == null)
            {
                throw new ArgumentNullException("No se puede buscar un partido con una selección nula");
            }
            var grupo = new GrupoSeleccion{ Nombre = nombreGrupo };
            if (!grupo.ValidarGrupo())
            {
                throw new ArgumentNullException("Grupo no valido");
            }
            try
            {
                var partidos = db.Partidos.Include(partido => partido.Seleccion1).ThenInclude(seleccion => seleccion._Pais).Include(partido => partido.Seleccion2).ThenInclude(seleccion => seleccion._Pais).ToList();
                //return new list with only partidos of group grupo
                return partidos.Where(partido => partido.Seleccion1.Grupo.CompareTo(grupo) == 0);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }

        public IEnumerable<Seleccion> SeleccionPorGrupo(string? nombreGrupo)
        {
            if (nombreGrupo == null)
            {
                throw new ArgumentNullException("No se puede buscar un partido con un grupo nulo");
            }
            var grupo = new GrupoSeleccion { Nombre = nombreGrupo };
            if (!grupo.ValidarGrupo())
            {
                throw new ArgumentNullException("Grupo no valido");
            }
            try
            {
                var selecciones = db.Selecciones.Include(seleccion => seleccion._Pais).Where(seleccion => seleccion.Grupo.Nombre == grupo.Nombre).ToList();
                //return new list with only partidos of group grupo
                return selecciones;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al buscar el partido", ex);
            }
        }

        public IEnumerable<Partido> GetPartidoBy(GrupoSeleccion grupoSeleccion, string codigoISO, string nombreParticipante, DateTime? fecha1, DateTime? fecha2)
        {
            throw new NotImplementedException();
        }
    }
    
}
