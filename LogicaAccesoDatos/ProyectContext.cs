﻿using LogicaDatos.Datos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Runtime.ConstrainedExecution;
using System.Text;

namespace LogicaAccesoDatos
{
    public class ProyectContext : DbContext
    {
        public DbSet<Pais> Paises { get; set; }

        public DbSet<Region> Regiones { get; set; }

        public DbSet<Seleccion> Selecciones { get; set; }

        public DbSet<Fase> Fases { get; set; }

        public DbSet<Grupo> FasesGrupos { get; set; }

        public DbSet<Eliminatoria> FasesEliminatoria { get; set; }

        public DbSet<FaseSeleccion> FaseSelecciones { get; set; }
        

        public DbSet<Partido> Partidos { get; set; }

        public ProyectContext(DbContextOptions<ProyectContext> options) : base(options)
        {
            
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server=(localDb)\MsSqlLocalDb;Database=MundialDBObligatorio;Integrated Security=true;");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Pais>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Nombre).IsRequired();
                entity.Property(e => e.Codigo).IsRequired();
                entity.Property(e => e.Poblacion).IsRequired();
                entity.Property(e => e.PBI).IsRequired();
                entity.Property(e => e.Imagen).IsRequired();
                entity.HasIndex(e => e.Nombre).IsUnique();
                entity.HasIndex(e => e.Codigo).IsUnique();
                entity.HasOne(e => e._Region).WithMany().HasForeignKey(e => e.RegionID).IsRequired();
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Nombre).IsRequired();
                entity.HasIndex(e => e.Nombre).IsUnique();
            });

            modelBuilder.Entity<Seleccion>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Nombre).IsRequired();
                entity.Property(e => e.Email).IsRequired();
                entity.Property(e => e.Telefono).IsRequired();
                entity.Property(e => e.CantApostadores).IsRequired();
                entity.HasIndex(e => e.Nombre).IsUnique();
                entity.HasOne(e => e._Pais).WithMany().HasForeignKey(e => e.PaisId).IsRequired();
                entity.OwnsOne(e => e.Grupo, e =>
                {
                    e.Property(e => e.Nombre).HasColumnName("Grupo").IsRequired();
                });
            });

            modelBuilder.Entity<Fase>(entity =>
            {
                entity.HasKey(fase => fase.ID);
                entity.HasMany(f => f.Partidos).WithOne();
            });

            modelBuilder.Entity<FaseSeleccion>(entity =>
            {
                entity.HasKey(faseSeleccion => new { faseSeleccion.SeleccionID, faseSeleccion.FaseID });
            });

            modelBuilder.Entity<Eliminatoria>(entity =>
            {
                entity.HasOne(e => e.Ganador).WithMany().HasForeignKey(e => e.GanadorID);
            });


            modelBuilder.Entity<Partido>(entity =>
            {
                entity.HasKey(e => e.ID);
                entity.HasOne(e => e.Seleccion1).WithMany().HasForeignKey(e => e.Seleccion1ID).IsRequired();
                entity.HasOne(e => e.Seleccion2).WithMany().HasForeignKey(e => e.Seleccion2ID).IsRequired();
                entity.Property(e => e.FechaYHora).IsRequired();
                entity.OwnsOne(e => e.Resultado1, e =>
                {
                    e.Property(e => e.GolesSeleccion).HasColumnName("Goles_Seleccion1");
                    e.Property(e => e.CantAmarillas).HasColumnName("Amarillas_Seleccion1");
                    e.Property(e => e.CantRojas).HasColumnName("Roja_Seleccion1");
                });
                entity.OwnsOne(e => e.Resultado2, e =>
                {
                    e.Property(e => e.GolesSeleccion).HasColumnName("Goles_Seleccion2");
                    e.Property(e => e.CantAmarillas).HasColumnName("Amarillas_Seleccion2");
                    e.Property(e => e.CantRojas).HasColumnName("Roja_Seleccion2");
                });
            });


        }
    }
}
