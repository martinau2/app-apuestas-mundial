﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.Datos
{
    public class Rol
    {
        public int ID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        
    }
}
