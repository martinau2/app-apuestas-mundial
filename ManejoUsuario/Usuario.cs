﻿using LogicaDatos.Datos;
using SistemaMundial.LogicaNegocio.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ManejoUsuario
{
    public class Usuario: IValidable<Usuario>
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        [ForeignKey("_Rol")]
        public int RolID { get; set; }
        public Rol _Rol { get; set; }

        public bool ValidarEmail()
        {
            bool valido = false;
            if(Email != null)
            {
                if (Email.Contains("@"))
                    valido = true;
            }
            return valido;
        }
        

        public bool Validar() {
            bool valido = false;
            if (ValidarEmail()) {
                valido = true;
            }
            return valido;
        }

        public bool ValidarPassword()
        {
            bool valido = false;
            if (Password.Any(char.IsLower) && Password.Any(char.IsUpper) && Password.Any(char.IsDigit) && Password.Any(c => "!.,!".Contains(c)) && Password.Length >= 8)
            {
                valido = true;
            }
            return valido;
        }
    }
}
