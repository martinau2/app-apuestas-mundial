﻿using Dominio.InterfacesRepositorios;
using ManejoUsuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaDatos.InterfacesRepositorios
{
    public interface IRepositorioUsuario : IRepositorio<Usuario>
    {
        public Usuario FindByEmail(string email);

        public Usuario FindByUsername(string username);

        public Usuario Login(string Email, string Password);

        public Usuario Register(Usuario usuario);
    }
}
