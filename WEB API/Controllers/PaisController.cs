﻿using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEB_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaisController : ControllerBase
    {
        private IRepositorioPais repositorioPais;
        public PaisController(IRepositorioPais repositorioPais)
        {
            this.repositorioPais = repositorioPais;
        }
        // GET: api/<SeleccionController>
        [HttpGet]
        public ActionResult<Seleccion> Get()
        {
            try
            {
                var paises = repositorioPais.FindAll().ToList();
                if (paises == null)
                {
                    return NotFound();
                }
                if (paises.Count() == 0)
                {
                    return StatusCode(404, "Selecciones esta vacio");
                }
                return Ok(paises);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
