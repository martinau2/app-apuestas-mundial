﻿using LogicaAccesoDatos;
using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using WEB_API.DTOS;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEB_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PartidoController : ControllerBase
    {
        private IRepositorioPartido repositorioPartido;
        public PartidoController(IRepositorioPartido repositorioPartido)
        {
            this.repositorioPartido = repositorioPartido;
        }
        
        // GET: api/<PartidoController>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }

        // GET api/<PartidoController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<PartidoController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<PartidoController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<PartidoController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        
        [HttpGet("GetPartidos")]
        public IActionResult GetPartidoBy([FromBody] GetPartidoByDTO getPartidoByDTO)
        {
            var grupo = new GrupoSeleccion() { Nombre = getPartidoByDTO.GrupoSeleccion };
            if (getPartidoByDTO.GrupoSeleccion == null)
            {
                grupo = null;
            }
            try
            {
                var partidos = repositorioPartido.GetPartidoBy(grupo
                    ,getPartidoByDTO.CodigoISO,getPartidoByDTO.NombreParticipante
                    ,getPartidoByDTO.Fecha1
                    ,getPartidoByDTO.Fecha2).ToList();
                if (partidos == null)
                {
                    return NotFound();
                }
                return Ok(partidos);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
