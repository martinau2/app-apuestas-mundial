﻿using LogicaAccesoDatos;
using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEB_API.Controllers
{
    [Route("api/Grupos")]
    [ApiController]
    public class GrupoController : ControllerBase
    {
        private IRepositorioGrupo repositorioGrupo;
        public GrupoController(IRepositorioGrupo repositorioGrupo)
        {
            this.repositorioGrupo = repositorioGrupo;
        }
        // GET: api/Grupos
        [HttpGet]
        public ActionResult<Grupo> Get()
        {
            try
            {
                var grupos = repositorioGrupo.FindAll();
                if (grupos == null)
                {
                    return NotFound();
                }
                if (grupos.Count() == 0)
                {
                    return StatusCode(404, "Grupos esta vacio");
                }
                return Ok(grupos);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET api/Grupos/5
        [HttpGet("{id}")]
        public ActionResult<Grupo> Get(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("No puede pasar null");
                }
                var seleccion = repositorioGrupo.FindById((int)id);
                if (seleccion == null)
                {
                    return NotFound();
                }
                return Ok(seleccion);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // POST api/Grupos
        [HttpPost]
        public ActionResult<Grupo> Post([FromBody] Grupo grupo)
        {
            try
            {
                if (grupo == null)
                {
                    return BadRequest("No puede pasar null");
                }
                if (repositorioGrupo.Add(grupo))
                {
                    return Ok(grupo);
                }
                return Conflict(grupo);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // PUT api/Grupos
        [HttpPut()]
        public ActionResult<Grupo> Put([FromBody] Grupo value)
        {
            try
            {
                if (value == null)
                {
                    return BadRequest("No puede pasar null");
                }
                if (repositorioGrupo.Update(value))
                {
                    return Ok(value);
                }
                return Conflict(value);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // DELETE api/Grupos/5
        [HttpDelete("{id}")]
        public ActionResult<Grupo> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("No puede pasar null");
                }
                if (repositorioGrupo.Remove((int)id))
                {
                    return NoContent();
                }
                return Conflict();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        // GET: api/Grupos/idGrupo/partidos
        [HttpGet("{idGrupo}/partido")]
        public ActionResult<Partido> GetPartidosDeGrupo(int? idGrupo)
        {
            try
            {
                var grupo = repositorioGrupo.FindById((int)idGrupo);
                if (grupo == null)
                {
                    return NotFound();
                }
                var partidos = grupo.Partidos;
                return Ok(partidos);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/Grupos/partidos/{idPartido}
        [HttpGet("{idGrupo}/partido/{idPartido}")]
        public ActionResult<Partido> GetPartido(int? idPartido)
        {
            try
            {
                var Partido = repositorioGrupo.FindByIdPartido(idPartido);
                return Ok(Partido);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/Grupos/partidos/{idPartido}
        [HttpGet("{idGrupo}/partido/grupo/{nombreGrupo}")]
        public ActionResult<Partido> GetPartidoPorGrupo(string? nombreGrupo)
        {
            try
            {
                var Partidos = repositorioGrupo.PartidosPorGrupo(nombreGrupo);
                return Ok(Partidos);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // POST: api/Grupos/{idGrupo}/partido
        [HttpPost("{idGrupo}/partido")]
        public ActionResult<Partido> PostPartidoAGrupo(int? idGrupo, [FromBody] Partido partido)
        {
            try
            {
                if (partido == null || idGrupo == null)
                {
                    return BadRequest("No puede pasar null");
                }
                var grupo = repositorioGrupo.FindById((int)idGrupo);
                if (grupo == null)
                {
                    return BadRequest("No se puede encontrar ese grupo");
                }

                if (repositorioGrupo.AddPartido(partido,idGrupo))
                {
                    return Ok(partido);

                }
                return Conflict(partido);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // PUT: api/Grupos/partido
        [HttpPut("{idGrupo}/partido")]
        public ActionResult<Partido> UpdatePartido([FromBody] Partido partido)
        {
            try
            {

                if (repositorioGrupo.UpdatePartido(partido))
                {
                    return Ok(partido);

                }
                return Conflict(partido);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // DELETE: api/Grupos/partido
        [HttpDelete("{idGrupo}/partido/{idPartido}")]
        public ActionResult<Partido> DeletePartido(int? idPartido)
        {
            try
            {
                if (idPartido == null)
                {
                    return BadRequest("No puede pasar null");
                }
                if (repositorioGrupo.RemovePartido((int)idPartido))
                {
                    return NoContent();
                }
                return Conflict();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // PUT: api/Grupos/partido
        [HttpPut("{idGrupo}/partido/{idPartido}/resultado")]
        public ActionResult<Partido> UpdateResultadosPartido([FromBody] ResultadoPartido[] Resultados, int? idPartido)
        {
            if(Resultados == null || Resultados.Length < 2)
            {
                return BadRequest("No se encontraron dos Resultados");
            }
            var ResultadoSeleccion1 = Resultados[0];
            var ResultadoSeleccion2 = Resultados[1];
            if(ResultadoSeleccion1 == null || ResultadoSeleccion2 == null)
            {
                return BadRequest("No se encontraron dos Resultados Correctos");
            }

            try
            {
                var partido = repositorioGrupo.FindByIdPartido(idPartido);
                if (partido == null)
                {
                    return BadRequest("No se puede encontrar ese partido");
                }

                ResultadoSeleccion1.CalcularRojas();
                ResultadoSeleccion2.CalcularRojas();
                
                partido.Resultado1 = ResultadoSeleccion1;
                partido.Resultado2 = ResultadoSeleccion2;
                
                partido.CalcularPuntajePartido();
                if (repositorioGrupo.UpdatePartido(partido))
                {
                    return Ok(partido);
                }
                return Conflict(partido);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        


    }
}
