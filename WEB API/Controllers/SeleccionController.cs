﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using System;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WEB_API.Controllers
{
    [Route("api/Seleccion")]
    [ApiController]
    public class SeleccionController : ControllerBase
    {
        private IRepositorioSeleccion repositorioSeleccion;
        public SeleccionController(IRepositorioSeleccion repositorioSeleccion)
        {
            this.repositorioSeleccion = repositorioSeleccion;
        }
        // GET: api/<SeleccionController>
        [HttpGet]
        public ActionResult<Seleccion> Get()
        {
            try
            {
                var selecciones = repositorioSeleccion.FindAll();
                if (selecciones == null)
                {
                    return NotFound();
                }
                if(selecciones.Count() == 0)
                {
                    return StatusCode(404, "Selecciones esta vacio");
                }
                return Ok(selecciones);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET api/<SeleccionController>/5
        [HttpGet("{id}")]
        public ActionResult<Seleccion> Get(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("No puede pasar null");
                }
                var seleccion = repositorioSeleccion.FindById((int)id);
                if (seleccion == null)
                {
                    return NotFound();
                }
                return Ok(seleccion);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        // POST api/<SeleccionController>
        [HttpPost]
        public ActionResult<Seleccion> Post([FromBody] Seleccion value)
        {
            try
            {
                if (value == null)
                {
                    return BadRequest("No puede pasar null");
                }
                if (repositorioSeleccion.Add(value))
                {
                    return Ok(value);
                }
                return Conflict(value);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }


        // PUT api/<SeleccionController>/5
        [HttpPut("{id}")]
        public ActionResult<Seleccion> Put(int id,[FromBody] Seleccion value)
        {
            try
            {
                if (value == null)
                {
                    return BadRequest("No puede pasar null");
                }
                else
                {
                    value.ID = id;
                }
                if (repositorioSeleccion.Update(value))
                {
                    return Ok(value);
                }
                return Conflict(value);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // DELETE api/<SeleccionController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("No puede pasar null");
                }
                if (repositorioSeleccion.Remove((int)id))
                {
                    return NoContent();
                }
                return Conflict();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/<SeleccionController>/puntaje/5
        [HttpGet("puntaje/{id}")]
        public ActionResult<int> GetPuntaje(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("No puede pasar null");
                }
                var puntaje = repositorioSeleccion.PuntajePorSeleccion((int)id);
                return Ok(puntaje);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/<SeleccionController>/golesfavor/5
        [HttpGet("golesfavor/{id}")]
        public ActionResult<int> GetGolesFavor(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("No puede pasar null");
                }
                var golesFavor = repositorioSeleccion.GolesAFavorPorSeleccion((int)id);
                return Ok(golesFavor);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/<SeleccionController>/golescontra/5
        [HttpGet("golescontra/{id}")]
        public ActionResult<int> GetGolesContra(int? id)
        {
            try
            {
                if (id == null)
                {
                    return BadRequest("No puede pasar null");
                }
                var golesContra = repositorioSeleccion.GolesAEncontraPorSeleccion((int)id);
                return Ok(golesContra);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        // GET: api/<SeleccionController>/selecciondegrupo/A
        [HttpGet("selecciondegrupo/{grupo}")]
        public ActionResult<Seleccion> GetSeleccionDeGrupo(string grupo)
        {
            try
            {
                if (grupo == null)
                {
                    return BadRequest("No puede pasar null");
                }
                var grupoObj = new GrupoSeleccion() { Nombre = grupo };
                if (!grupoObj.ValidarGrupo())
                {
                        return BadRequest("No es un grupo valido");
                    
                }
                var seleccion = repositorioSeleccion.SeleccionesDeGrupo(grupoObj);
                if (seleccion == null)
                {
                    return NotFound();
                }
                return Ok(seleccion);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }


}
