﻿using LogicaDatos.Datos;
using System;

namespace WEB_API.DTOS
{
    public class GetPartidoByDTO
    {
        public string GrupoSeleccion { get; set; }
        public string CodigoISO { get; set; }
        public string NombreParticipante { get; set; }
        public DateTime? Fecha1 { get; set; } = null;
        public DateTime? Fecha2 { get; set; } = null;

    }
}
