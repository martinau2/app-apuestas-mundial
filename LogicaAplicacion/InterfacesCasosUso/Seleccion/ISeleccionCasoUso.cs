﻿using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaAplicacion.InterfacesCasosUso.Seleccion
{
    internal interface ISeleccionCasoUso
    {
        public IEnumerable<Region> FindAll();
    }
}
