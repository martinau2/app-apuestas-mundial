﻿using ManejoUsuario;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaAplicacion.InterfacesCasosUso.UsuarioI
{
    public interface IUsuarioCasoUso
    {
        public IEnumerable<Usuario> FindAll();

        public Usuario FindByEmail(string Email);

        public Usuario Login(string Email, string Password);

        public Usuario Register(Usuario usuario);
    }
}
