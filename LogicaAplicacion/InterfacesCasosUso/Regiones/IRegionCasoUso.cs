﻿using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaAplicacion.InterfacesCasosUso.Regiones
{
    public interface IRegionCasoUso
    {
        public IEnumerable<Region> FindAll();

        public Region FindById(int id);
    }
}
