﻿using LogicaDatos.Datos;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaAplicacion.InterfacesCasosUso
{
    public interface IPaisCasoUso
    {
        public void InsertarPais(Pais pais);

        public void EliminarPais(Pais pais);

        public void ModificarPais(Pais pais);

        public IEnumerable<Pais> FindAll();

        public Pais FindById(int id);

        public IEnumerable<Pais> FindForSearch(int? id, string codigo, int? region);
    }
}
