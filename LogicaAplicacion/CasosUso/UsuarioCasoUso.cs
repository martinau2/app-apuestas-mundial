﻿using LogicaAplicacion.InterfacesCasosUso.UsuarioI;
using LogicaDatos.InterfacesRepositorios;
using ManejoUsuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogicaAplicacion.CasosUso
{
    public class UsuarioCasoUso : IUsuarioCasoUso
    {
        private IRepositorioUsuario _repositorioUsuario;
        public UsuarioCasoUso(IRepositorioUsuario repositorioUsuario)
        {
            _repositorioUsuario = repositorioUsuario;
        }
        public IEnumerable<Usuario> FindAll()
        {
            return _repositorioUsuario.FindAll().ToList();
        }

        public Usuario FindByEmail(string email)
        {
            return _repositorioUsuario.FindByEmail(email);
        }

        public Usuario Login(string Email, string Password)
        {
            return _repositorioUsuario.Login(Email, Password);
        }
    

        public Usuario Register(Usuario usuario)
        {
            return _repositorioUsuario.Register(usuario);
        }
    }
}
