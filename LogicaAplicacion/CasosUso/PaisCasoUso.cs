﻿using LogicaAplicacion.InterfacesCasosUso;
using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaAplicacion.CasosUso
{
    public class PaisCasoUso : IPaisCasoUso
    {
        //Inyecto dependencia
        private IRepositorioPais _repoPais;
        public PaisCasoUso(IRepositorioPais repoPais)
        {
            _repoPais = repoPais;
        }
        public void InsertarPais(Pais unPais)
        {

            if (unPais == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            try
            {
                _repoPais.Add(unPais);
            }
            catch (InvalidOperationException ex)
            {
                throw (new Exception(ex.Message + "Dando de alta pais"));
            }
        }

        public void EliminarPais(Pais pais)
        {
            if (pais == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            try
            {
                _repoPais.Remove(pais.ID);
            }
            catch (InvalidOperationException ex)
            {
                throw (new Exception(ex.Message + "Dando de alta pais"));
            }
        }

        public void ModificarPais(Pais pais)
        {
            if (pais == null)
            {
                throw new ArgumentNullException("No se puede agregar un objeto nulo");
            }
            try
            {
                _repoPais.Update(pais);
            }
            catch (InvalidOperationException ex)
            {
                throw (new Exception(ex.Message + "Dando de alta pais"));
            }
        }

        public IEnumerable<Pais> FindAll()
        {
            return _repoPais.FindAll();
        }

        public Pais FindById(int id)
        {
            return _repoPais.FindById(id);
        }

        public IEnumerable<Pais> FindForSearch(int? id, string codigo, int? region)
        {
            return _repoPais.FindForSearch(id, codigo, region);
        }
    }
}
