﻿using LogicaAplicacion.InterfacesCasosUso.Regiones;
using LogicaDatos.Datos;
using LogicaDatos.InterfacesRepositorios;
using System;
using System.Collections.Generic;
using System.Text;

namespace LogicaAplicacion.CasosUso.Regiones
{
    public class RegionCasoUso : IRegionCasoUso
    {
        private IRepositorioRegion _repositorioRegion;

        public RegionCasoUso(IRepositorioRegion repositorioRegion)
        {
            _repositorioRegion = repositorioRegion;
        }

        public IEnumerable<Region> FindAll()
        {
            return _repositorioRegion.FindAll();
        }

        public Region FindById(int id)
        {
            return _repositorioRegion.FindById(id);
        }
    }
}
